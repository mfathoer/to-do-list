package com.mfathoer.todolist.view.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mfathoer.todolist.core.vo.LoadResult
import com.mfathoer.todolist.core.domain.Todo
import com.mfathoer.todolist.data.TodoRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class HomeViewModel(private val repository: TodoRepository) : ViewModel() {

    private val _todos: MutableStateFlow<LoadResult<List<Todo>>> =
        MutableStateFlow(LoadResult.Loading())

    val todos: StateFlow<LoadResult<List<Todo>>>
        get() = _todos

    fun getAllTodos() = viewModelScope.launch {
        try {
            repository.getAllTodos().collect { list ->
                _todos.value = list
            }
        } catch (e: Exception) {
            _todos.value = LoadResult.Error(message = e.message.toString())
        }
    }
}