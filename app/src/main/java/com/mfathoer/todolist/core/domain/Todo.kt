package com.mfathoer.todolist.core.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Todo(
    val id: Int = 0,
    val title: String,
    val description: String,
    val createdAt: Long,
    val updatedAt: Long
) : Parcelable
