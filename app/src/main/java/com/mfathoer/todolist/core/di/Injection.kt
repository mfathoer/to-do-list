package com.mfathoer.todolist.core.di

import android.content.Context
import com.mfathoer.todolist.core.ui.ViewModelFactory
import com.mfathoer.todolist.data.TodoRepository
import com.mfathoer.todolist.data.local.LocalDatabase
import com.mfathoer.todolist.data.local.TodoLocalDataSource
import kotlinx.coroutines.Dispatchers

/**
 * This class is used for manual dependency injection.
 */
object Injection {

    private fun provideIODispatcher() = Dispatchers.IO

    private fun provideLocalDatabase(context: Context) = LocalDatabase.getInstance(context)

    private fun provideTodoLocalDataSource(context: Context) =
        TodoLocalDataSource.getInstance(
            provideLocalDatabase(context).getTodoDao(),
            provideIODispatcher()
        )

    private fun provideTodoRepository(context: Context) =
        TodoRepository.getInstance(provideTodoLocalDataSource(context), provideIODispatcher())

    fun provideViewModelFactory(context: Context) =
        ViewModelFactory.getInstance(provideTodoRepository(context))
}