package com.mfathoer.todolist.view.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mfathoer.todolist.core.domain.Todo
import com.mfathoer.todolist.data.TodoRepository
import kotlinx.coroutines.launch

class DetailViewModel(private val repository: TodoRepository) : ViewModel() {

    fun addTodo(todo: Todo) = viewModelScope.launch {
        repository.addTodo(todo)
    }

    fun updateTodo(todo: Todo) = viewModelScope.launch {
        repository.updateTodo(todo)
    }

    fun deleteTodo(todo: Todo) = viewModelScope.launch {
        repository.deleteTodo(todo)
    }

    // This variable is used for determine condition, whether in creating to-do or updating to-do condition
    private var _isOnUpdatingTodo = false

    val isOnUpdatingTodo get() = _isOnUpdatingTodo

    fun setIsOnUpdatingTodo(state: Boolean) {
        _isOnUpdatingTodo = state
    }
}