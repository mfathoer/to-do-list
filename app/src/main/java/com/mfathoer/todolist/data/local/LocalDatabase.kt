package com.mfathoer.todolist.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mfathoer.todolist.data.local.entity.TodoEntity


@Database(entities = [TodoEntity::class], version = 1, exportSchema = false)
abstract class LocalDatabase : RoomDatabase() {

    abstract fun getTodoDao(): TodoDao

    companion object {
        @Volatile
        private var instance: LocalDatabase? = null

        fun getInstance(context: Context): LocalDatabase = instance ?: synchronized(this) {
            instance ?: buildDatabase(context).apply { instance = this }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, LocalDatabase::class.java, "room_db")
                .build()

    }

}