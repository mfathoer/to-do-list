package com.mfathoer.todolist.core.utils

import com.mfathoer.todolist.core.domain.Todo
import com.mfathoer.todolist.data.local.entity.TodoEntity

/**
 * This class is used for mapping the to-do data class. The intent of changing to-do data class
 * whenever moving from between layer is to making the code more easy to maintained and
 * become more robust
 *
 */
object DataMapper {

    fun mapTodoEntitiesToDomain(list: List<TodoEntity>): List<Todo> {
        val newList = ArrayList<Todo>()

        list.map { todoEntity ->
            val todo = Todo(
                id = todoEntity.id,
                title = todoEntity.title,
                description = todoEntity.description,
                createdAt = todoEntity.createdAt,
                updatedAt = todoEntity.updatedAt
            )
            newList.add(todo)
        }

        return newList
    }


    fun mapTodoDomainToEntity(todo: Todo): TodoEntity {
        return TodoEntity(
            id = todo.id,
            title = todo.title,
            description = todo.description,
            createdAt = todo.createdAt,
            updatedAt = todo.updatedAt
        )
    }

}