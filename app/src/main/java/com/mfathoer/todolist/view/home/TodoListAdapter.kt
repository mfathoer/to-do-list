package com.mfathoer.todolist.view.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mfathoer.todolist.R
import com.mfathoer.todolist.core.domain.Todo
import com.mfathoer.todolist.databinding.ItemTodoBinding

class TodoListAdapter : ListAdapter<Todo, TodoListAdapter.TodoViewHolder>(TODO_ITEM_COMPARATOR) {


    var onItemClicked: ((Todo) -> Unit)? = null


    inner class TodoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemTodoBinding.bind(view)

        fun bind(data: Todo) {

            binding.tvTitle.text = data.title
            binding.tvDescription.text = data.description

            binding.root.setOnClickListener {
                onItemClicked?.invoke(data)
            }
        }
    }

    companion object {
        private val TODO_ITEM_COMPARATOR = object : DiffUtil.ItemCallback<Todo>() {
            override fun areItemsTheSame(oldItem: Todo, newItem: Todo): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Todo, newItem: Todo): Boolean {
                return oldItem.id == newItem.id && oldItem.title == newItem.title && newItem.description == oldItem.description
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_todo, parent, false)
        return TodoViewHolder(view)
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}