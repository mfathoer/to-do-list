package com.mfathoer.todolist.view.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.mfathoer.todolist.R
import com.mfathoer.todolist.core.di.Injection
import com.mfathoer.todolist.core.domain.Todo
import com.mfathoer.todolist.databinding.FragmentDetailBinding

class DetailFragment : Fragment(), View.OnClickListener {

    private var _binding: FragmentDetailBinding? = null

    private val binding get() = _binding

    private val viewModel by viewModels<DetailViewModel> {
        Injection.provideViewModelFactory(requireContext())
    }

    private val args by navArgs<DetailFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnBack?.setOnClickListener(this)
        binding?.btnDelete?.setOnClickListener(this)
        binding?.btnAddOrUpdate?.setOnClickListener(this)

        populateData()
        initViews()
    }

    private fun populateData() {
        if (args.todo != null) {
            binding?.etTitle?.setText(args.todo?.title)
            binding?.etDescription?.setText(args.todo?.description)

            viewModel.setIsOnUpdatingTodo(true)
        } else {
            viewModel.setIsOnUpdatingTodo(false)
        }
    }

    private fun initViews() {
        if (viewModel.isOnUpdatingTodo) {
            binding?.btnDelete?.visibility = View.VISIBLE
            binding?.btnAddOrUpdate?.text = getString(R.string.update)
        } else {
            binding?.btnDelete?.visibility = View.GONE
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back -> {
                activity?.onBackPressed()
            }

            R.id.btn_delete -> {
                args.todo?.let { viewModel.deleteTodo(it) }
                activity?.onBackPressed()
            }

            R.id.btn_add_or_update -> {
                if (isUserInputValidated()) {
                    if (viewModel.isOnUpdatingTodo) {
                        if (isUserInputValidated()) {
                            args.todo?.let {
                                val title = binding?.etTitle?.text.toString()
                                val description = binding?.etDescription?.text.toString()

                                val todo = Todo(
                                    id = it.id,
                                    title = title,
                                    description = description,
                                    createdAt = it.createdAt,
                                    updatedAt = System.currentTimeMillis()
                                )

                                viewModel.updateTodo(todo)
                            }
                        }
                    } else {
                        val title = binding?.etTitle?.text.toString()
                        val description = binding?.etDescription?.text.toString()

                        val todo = Todo(
                            title = title,
                            description = description,
                            createdAt = System.currentTimeMillis(),
                            updatedAt = System.currentTimeMillis()
                        )
                        viewModel.addTodo(todo)
                    }
                    activity?.onBackPressed()
                }
            }
        }
    }

    private fun isUserInputValidated(): Boolean {
        var validated = true
        val title = binding?.etTitle?.text
        val description = binding?.etDescription?.text

        if (title.isNullOrBlank()) {
            validated = false
            binding?.etTitle?.error = getString(R.string.empty_et_title)
        }

        if (description.isNullOrBlank()) {
            validated = false
            binding?.etDescription?.error = getString(R.string.empty_et_desc)
        }

        return validated
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}

