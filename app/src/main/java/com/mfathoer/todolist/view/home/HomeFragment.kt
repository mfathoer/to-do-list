package com.mfathoer.todolist.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.mfathoer.todolist.R
import com.mfathoer.todolist.core.vo.LoadResult
import com.mfathoer.todolist.core.di.Injection
import com.mfathoer.todolist.core.domain.Todo
import com.mfathoer.todolist.core.utils.showLongToastMessage
import com.mfathoer.todolist.databinding.FragmentHomeBinding
import kotlinx.coroutines.flow.collect


class HomeFragment : Fragment(), View.OnClickListener {

    private var _binding: FragmentHomeBinding? = null

    private val binding get() = _binding

    private val viewModel by viewModels<HomeViewModel> {
        Injection.provideViewModelFactory(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.fabAdd?.setOnClickListener(this)

        viewModel.getAllTodos()

        val todoListAdapter = TodoListAdapter()

        todoListAdapter.onItemClicked = { todo ->
            goToDetailFragment(todo)
        }

        binding?.rvTodos?.layoutManager = LinearLayoutManager(requireContext())
        binding?.rvTodos?.adapter = todoListAdapter

        lifecycleScope.launchWhenResumed {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                viewModel.todos.collect { loadResult ->
                    when (loadResult) {
                        is LoadResult.Success -> {
                            showLoadingState(false)

                            todoListAdapter.submitList(loadResult.data)

                            if (loadResult.data.isNullOrEmpty()) {
                                showEmptyListState(true)
                            } else {
                                showEmptyListState(false)
                            }
                        }
                        is LoadResult.Loading -> {
                            showLoadingState(true)
                        }
                        is LoadResult.Error -> {
                            showEmptyListState(true)
                            showLoadingState(false)

                            context?.showLongToastMessage(loadResult.message.toString())
                        }
                    }
                }
            }
        }

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.fab_add -> goToDetailFragment()
        }
    }

    private fun goToDetailFragment(todo: Todo? = null) {
        val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment()
        action.todo = todo

        findNavController().navigate(action)
    }

    private fun showLoadingState(state: Boolean) {
        if (state) {
            binding?.progressBar?.visibility = View.VISIBLE
        } else {
            binding?.progressBar?.visibility = View.GONE
        }
    }

    private fun showEmptyListState(state: Boolean) {
        if (state) {
            binding?.tvEmptyState?.visibility = View.VISIBLE
        } else {
            binding?.tvEmptyState?.visibility = View.GONE
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}